var app = require('express')();

var server = require('http').Server(app);
var io = require('socket.io')(server);

app.set('port',process.env.PORT || 3000);
var clients = [];

io.on("connection", function(socket){
    var currentUser;
    console.log("User");
    socket.on("USER_CONNECT", function (){
        console.log("User connected");
        socket.broadcast.emit("USER_DISCONNECTED" , currentUser);
    });

    socket.on("PLAY", function(data) {
    });

    socket.on("MOVE",function (data){
    });
    socket.on("user:login",function (data){
        console.log("get message is 'user:login' ");
        console.log("email = " + data['email']+ data['pass']);

        var test = {Vorname : "Bongsang" , Nachname :"Kim"};
        socket.emit("RETURN_DATA",  test);
    });


    socket.on("disconnect", function(){
        console.log("User disconnected");
        socket.broadcast.emit("USER_DISCONNECTED" , currentUser);
    });
});

server.listen(app.get('port'), function() {
    console.log("------------- SERVER IS RUNNING -----------");
});

/*var http = require('http');

 function onRequest(request , response) {
 console.log("A user made a request" );
 response.writeHead(200,{"Context-Type":"text/plain"})
 response.write("Here is some data");
 response.end();
 }

 http.createServer(onRequest).listen(30000);
 console.log("Server is now runing........");*/